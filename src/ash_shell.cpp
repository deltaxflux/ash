#include <iostream>
#include <cstring>
#include <deltax/ash.h>
#include <pwd.h>
#include <sys/utsname.h>

#define RESET "\033[30m"
#define RED "\033[31m"
#define GREEN "\033[32m"
#define YELLOW "\033[33m"
#define BLUE "\033[34m"
#define PURPLE "\033[35m"
#define WHITE "\033[37m"

std::string get_username(uid_t uid)
{
    passwd* ret = getpwuid(uid);
    if (ret == NULL)
        return "";
    return std::string(ret->pw_name);
}

std::string get_tip()
{
    char hostname[65];
    gethostname(hostname, 65);
    char* curdir = getcwd(NULL, 0);
    std::string ret = PURPLE + get_username(getuid()) + WHITE +" at " + YELLOW + hostname + WHITE + " in ";

    int lastpos = 0;
    for (auto i = 0u; i < strlen(curdir); i++) {
        if (curdir[i] == '/')
            lastpos = i;
    }
    std::string where = std::string(curdir).substr(lastpos + 1);

    if (where == "")
        where = "/";
    ret += RED;
    ret += "[" + where + "]";
    ret += WHITE;
    ret += ((geteuid() == 0) ? "\n# " : "\n$ ");
    free(curdir);
    return ret;
}

int ash_error(int error)
{
    switch (error) {
    case 1:
        break;
    case 100:
        printf("ash: command not found.\n");
        break;
    case 201:
    case 202:
        printf("ash: file doesn\'t exist.\n");
        break;
    case 300:
        printf("ash: environment error.\n");
        break;
    case 400:
    case 401:
    case 403:
        printf("ash: error present.\n");
        break;
    }
    return -1;
}

std::string string_trim(std::string s)
{
    if (s.empty()) {
        return s;
    }
    s.erase(0, s.find_first_not_of(" "));
    s.erase(s.find_last_not_of(" ") + 1);
    return s;
}

int shellfunc_cd(command_t cmdt)
{
    if (cmdt.arguments.size() == 1) {
        cmdt.arguments.push_back(".");
    }
    if (chdir(cmdt.arguments[1].c_str()) != 0) {
        perror("ash");
        return -1;
    }
    return 0;
}

int shellfunc_exit(command_t cmdt)
{
    exit(0);
    return 0;
}

int shellfunc_logout(command_t cmdt)
{
    return shellfunc_exit(cmdt);
}
